const parser = require('../parser');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const rimraf = require('rimraf');

const reportDir = path.resolve(__dirname, '..', 'testreports');
const sentDir = path.resolve(__dirname, '..', 'sent');

describe('parser', function() {
	it('should parse reports', function() {
		var reports = parser.getReports({
			reportDir: reportDir
		});

		assert(reports.length > 0);

		reports.forEach(function(report) {
			assert(report.filename);
			assert(report.reportname);
			assert(report.status);
			assert(report.time);
			assert(report.logs.length > 0);
			assert(report.logs[ 0 ]);
			assert(report.logs[ 0 ].level);
		}, this);
	});

	it('should get warnings', function() {
		rimraf.sync(sentDir);

		var warnings = parser.getWarnings({
			reportDir: reportDir
		});

		console.log(warnings);

		assert.equal(warnings.length, 3);
		assert.equal(warnings[0].messages.length, 1);
		assert.equal(warnings[1].messages.length, 1);
		assert.equal(warnings[2].messages.length, 4);
		
		assert.equal(
			warnings[2].messages[1],
			'log: Could not retrieve catalog from remote server: Net::ReadTimeout');

		// should not longer get warnings after marked as sent
		parser.markSent(warnings);

		warnings = parser.getWarnings({
			reportDir: reportDir
		});

		assert(warnings.length === 0);

		// cleanup
		rimraf.sync(sentDir);
	});
});
