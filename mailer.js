#!/usr/bin/env node

var config = require('./config'),
	fs = require('fs'),
	path = require('path'),
	nodemailer = require('nodemailer'),
	_ = require('underscore'),
	xoauth2 = require('xoauth2');

var generator = xoauth2.createXOAuth2Generator({
    user: config.mailer.oauthUser,
    clientId: config.mailer.clientId,
    clientSecret: config.mailer.clientSecret,
    refreshToken: config.mailer.refreshToken
});

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
		xoauth2: generator
    }
});

/**
 * @options {object} { subject, text, files[] }
 */
exports.sendFiles = function(options, cb) {
	console.log('mailer sendFiles', options);

	transporter.sendMail({
		from: config.mailer.oauthUser,
		to: options.recipents.join(','),
		subject: options.subject,
		text: options.text,
		attachments: _.map(options.files, function(item) {
			return {
				filename: path.basename(item),
				content: fs.createReadStream(item)
			};
		})
	}, cb);
};

exports.close = function() {
	transporter.close();
};
