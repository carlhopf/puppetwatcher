#!/usr/bin/env node
const app = require('./app');

exports.run = app.run;

if (require.main === module) {
	app.run();
}
