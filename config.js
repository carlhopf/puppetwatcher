const path = require('path');
const argv = require('minimist')(process.argv.slice(2));

var config = {
	mailer: {
		oauthUser: argv.oauthUser,
		clientId: argv.clientId,
		clientSecret: argv.clientSecret,
		refreshToken: argv.refreshToken,
	},

	mailTo: (argv.mailTo || '').split(','),
	reportDir: argv.reportDir || path.join(process.cwd(), 'testreports'),
	
	validStates: ['success', 'changed', 'unchanged'],
	logLevelsIncludeMail: [':err', 'err'],
};

if (!config.mailer.oauthUser) console.warn('oauthUser missing');
if (!config.mailer.refreshToken) console.warn('refreshToken missing');
if (!config.mailer.clientSecret) console.warn('clientSecret missing');
if (!config.mailer.clientId) console.warn('clientId missing');

module.exports = config;
