const config = require('./config');
const os = require('os');
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const _ = require('underscore');
const async = require('async');
const yaml = require('js-yaml');

const DIR_SENT = 'sent';
const DELAY_MAX = 3 * 3600 * 1000;

const YAML_TYPES = {
	'!ruby/sym': 'scalar',
	'!ruby/object': 'scalar',
	'!ruby/object:Puppet::Util::Log': 'mapping',
	'!ruby/object:Puppet::Transaction::Report': 'mapping',
	'!ruby/object:Puppet::Util::Metric': 'mapping',
	'!ruby/object:Puppet::Resource::Status': 'mapping',
	'!ruby/object:Puppet::Transaction::Event': 'mapping'
};

var SCHEMAS = [];

Object.keys(YAML_TYPES).forEach(function(key) {
	SCHEMAS.push(new yaml.Type(key, {
		kind: YAML_TYPES[ key ],
		construct: function (data) {
			//console.log('construct', typeof data, data);
			return data;
		},
		instanceOf: String
		// `represent` is omitted here. So, Space objects will be dumped as is.
		// That is regular mapping with three key-value pairs but with !space tag.
	}));
});

var SCHEMA = yaml.Schema.create(SCHEMAS);

var filter = function(item) {
	if (item === '.DS_Store') return false;
	return true;
};

var filterWarnings = function(item) {

};

var getSentFile = function(report) {
	return path.join(__dirname, DIR_SENT, report.host + '-' + report.reportname);
};

/**
 * @param {function} filterFn - Filter report, to instantly free memory.
 */
exports.getReports = function(options, filterFn) {
	options = options || {};

	var nodes = fs.readdirSync(config.reportDir).sort().filter(filter);
	var result = [];

	if (!nodes) return console.error('could not read reports');

	nodes.forEach(function(node) {
		// get latest report, filename is YYYY+MM+DD so can sort() by filename
		var reports = fs.readdirSync(path.join(config.reportDir, node))
			.sort().filter(filter);

		if (reports.length === 0)
			return console.log('no reports found for ' + node);

		var reportname = reports.pop();
		var filename = path.join(config.reportDir, node, reportname);

		var object = yaml.load(
			fs.readFileSync(filename, 'utf8'), { schema: SCHEMA });

		if (object.filename)
			throw new Error('already has filename');
		else
			object.filename = filename;

		if (object.reportname)
			throw new Error('already has reportname');
		else
			object.reportname = reportname;

		// filter
		if (filterFn && !filterFn(object)) {
			return;
		}

		result.push(object);
	});

	return result;
};

exports.getWarnings = function(options) {
	options = options || {};

	var result = [];

	exports.getReports(options, function(report) {
		var messages = [];
		var status = report.status;
		var host = report.host;

		// report has already been sent
		if (fs.existsSync(getSentFile(report))) {
			console.log('report ' + report.reportname +
				' for host ' + host + ' has already been sent');

			return false;
		}

		if (config.validStates.indexOf(status) === -1) {
			messages.push('status: ' + status);
		}

		// error log
		report.logs.forEach((log) => {
			if (config.logLevelsIncludeMail.indexOf(log.level) !== -1) {
				messages.push('log: ' + log.message);
			}
		});

		var delay = new Date().getTime() - new Date(report.time + '').getTime();

		if (delay > DELAY_MAX) {
			messages.push(
				'last run: ' + report.time +
				'(' + Math.floor(delay / (3600 * 1000)) + 'h ago)');
		}

		if (messages.length !== 0) {
			result.push({
				host: host,
				messages: messages,
				report: report,
				reportname: report.reportname,
				filename: report.filename
			});

			return true;
		}

		return false;
	});

	return result;
};

exports.markSent = function(warnings) {
	mkdirp.sync(path.join(__dirname, DIR_SENT));

	warnings.forEach(function(warning) {
		fs.writeFileSync(getSentFile(warning.report), 'ok');
	});
};
