#!/usr/bin/env node
const config = require('./config');
const mailer = require('./mailer');
const parser = require('./parser');

exports.run = function (cb) {
	cb = cb || function () {};

	var warnings = parser.getWarnings({});

	var mail = {
		files: [],
		message: '',
		subject: 'puppetwatcher'
	};

	warnings.forEach(function(warning) {
		mail.message +=
			warning.host + '\n' +
			'- ' + warning.messages.join('\n- ') + '\n' +
			'\n';

		mail.files.push(warning.filename);
	});

	if(warnings.length === 0) {
		return cb(null);
	}

	mailer.sendFiles({
		recipents: config.mailTo,
		subject: mail.subject,
		text: mail.message,
		files: mail.files
	}, function (err) {
		if (err) {
			console.warn('err', err);
		} else {
			parser.markSent(warnings);
		}

		cb(null);
	});
}
